# individual-proj-4
# Rust Lambda Function for AWS

This Rust code defines a simple Lambda function to be deployed on AWS. The function takes a `LambdaRequest` input, processes it, and returns a `LambdaResponse`. The function is designed to be invoked by AWS Lambda, and it can be integrated into AWS Step Functions for building serverless workflows.

## Functionality

The Lambda function defined in this codebase performs the following actions:

1. Receives a `LambdaRequest` containing a full name and an optional message.
2. Formats the full name into a greeting message.
3. Adds a message to the response based on the input message (if provided).
4. Returns a `LambdaResponse` containing the formatted full name and message.

![Step Function Diagram](individual-proj-4/stepfunctions_graph.png)


## Usage

### Prerequisites

- [Rust](https://www.rust-lang.org/tools/install)
- [AWS CLI](https://aws.amazon.com/cli/)
- An AWS account with appropriate IAM permissions

### Deployment Steps

1. Clone this repository to your local machine.
2. Update the `main.rs` file with your desired logic and functionality.
3. Build the Rust project:

   ```bash
   cargo build --release --target x86_64-unknown-linux-musl```


4. Create a ZIP file containing the built binary:
    ```bash
    zip -j lambda.zip ./target/x86_64-unknown-linux-musl/release/lambda_function```

5. Deploy the Lambda function using the AWS CLI:
    ```bash
    AWS_ACCOUNT_ID=<your-aws-account-id>
    aws lambda create-function \
    --function-name <function-name> \
    --runtime provided.al2023 \
    --role arn:aws:iam::$AWS_ACCOUNT_ID:role/<your-lambda-execution-role> \
    --zip-file fileb://lambda.zip \
    --description "Simple Rust function" \
    --timeout 5 \
    --handler main```

### Testing
Unit tests for the Lambda handler are defined in the tests module within main.rs. You can run the tests using the following command:
    ```bash
    cargo test```

Integration with AWS Step Functions
Once the Lambda function is deployed, you can integrate it into AWS Step Functions for orchestrating serverless workflows. Use the AWS Step Functions console or AWS SDK to define your state machine and include this Lambda function as one of the states.

For more information on AWS Lambda and Step Functions, refer to the AWS documentation.

Video: 

https://youtu.be/4PETrpgAd1U 




