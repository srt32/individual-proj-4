
/// returns metdata information about the Lambda runtime
pub fn runtime_release() -> &'static str {
    "AWS_Lambda_Rust/0.1.2 (/home/codespace/.rustup/toolchains/stable-x86_64-unknown-linux-gnu/bin/rustc/1.77.2-stable)"
}
